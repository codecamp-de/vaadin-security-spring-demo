# Spring Boot Security for Vaadin Demo

Demo application for [Spring Boot Security for Vaadin Flow](https://gitlab.com/codecamp-de/vaadin-security-spring).

## Used Versions

* Spring Boot: `2.3.0`
* Vaadin: `18.0.2`
* Spring Boot Security for Vaadin Flow `1.0.0`

## Used Authentication Method

Uses the standard authentication provided by the add-on:

* The login page is a regular Vaadin view.
* Authentication is internally performed using Spring Security's form-based authentication wrapped in a
  convenient Java API. Logout is provided similarly.
