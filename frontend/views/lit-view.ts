import { LitElement, html, css } from 'lit';
import { customElement } from 'lit/decorators.js';

import '@vaadin/button';
import '@vaadin/notification';
import '@vaadin/horizontal-layout';
import '@vaadin/vertical-layout';

import { Notification } from '@vaadin/notification';

import * as demoEndpoint from '../generated/DemoEndpoint';


@customElement("lit-view")
export class LitView
  extends LitElement
{

  render() {
    return html`
      <vaadin-vertical-layout theme="padding spacing" theme="dark">
        <vaadin-horizontal-layout theme="margin spacing">
          <vaadin-button theme="primary" @click=${this.clickHandlerOne}>
            Permit All
          </vaadin-button>
          <vaadin-button theme="primary" @click=${this.clickHandlerTwo}>
            Requires Authentication
          </vaadin-button>
          <vaadin-button theme="primary" @click=${this.clickHandlerThree}>
            Requires Admin Role
          </vaadin-button>
        </vaadin-horizontal-layout>
      </vaadin-vertical-layout>
    `;
  }

  private clickHandlerOne() {
    demoEndpoint.methodOne("NAME")
      .then(result => {
        this.showNotification(true, 'Success' );
      })
      .catch(error => {
        this.showNotification(false, 'Error: ' + error.message);
      });
  }

  private clickHandlerTwo() {
    demoEndpoint.methodTwo("NAME")
      .then(result => {
        this.showNotification(true, 'Success');
      })
      .catch(error => {
        this.showNotification(false, 'Error: ' + error.message);
      });
  }

  private clickHandlerThree() {
    demoEndpoint.methodThree("NAME")
      .then(result => {
        this.showNotification(true, 'Success');
      })
      .catch(error => {
        this.showNotification(false, 'Error: ' + error.message);
      });
  }

  private showNotification(success: boolean, msg: string)
  {
    const notify = new Notification();
    notify.renderer = root => {
      root.textContent = msg;
    };

    window.document.body.appendChild(notify);

    notify.setAttribute("theme", success ? 'success' : 'error');
    notify.position = 'bottom-start';
    notify.duration = 3000;
    notify.opened = true;

    notify.addEventListener('opened-changed', () => {
      window.document.body.removeChild(notify);
    });
  }

  // this allows the Lumo styles for standard HTML elements to be applied here like in a Java view
  createRenderRoot() {
    return this;
  }

}
