package de.codecamp.vaadin.security.spring.demo.security;


import de.codecamp.vaadin.security.spring.access.AccessContext;
import de.codecamp.vaadin.security.spring.access.AccessEvaluator;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;


public class CustomRouteAccessEvaluator
  implements
    AccessEvaluator
{

  @Override
  public boolean hasAccess(AccessContext accessContext)
  {
    // Register this evaluator as a Spring bean e.g. to make use of dependency injection.
    // VaadinSecurity can also be used here.
    return VaadinSecurity.check().hasRole("ADMIN");
  }

}
