package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessConfiguration;
import de.codecamp.vaadin.security.spring.authentication.AuthenticationChangeEvent;
import de.codecamp.vaadin.security.spring.authentication.AuthenticationChangeObserver;
import de.codecamp.vaadin.security.spring.authentication.VaadinAuthenticationService;
import org.springframework.security.core.context.SecurityContextHolder;


public class AppFrame
  extends
    Composite<VerticalLayout>
  implements
    RouterLayout,
    AuthenticationChangeObserver
{

  private Label userLabel;


  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();
    layout.setSizeFull();

    HorizontalLayout topBar = new HorizontalLayout();
    topBar.add(new Html("<b>Flow views</b>"));
    topBar.add(createLink("\u2022 Unsecured [Inline Access Control]", MainView.class));
    topBar.add(createLink("\u2022 Secured [Expression]", ExpressionView.class));
    topBar.add(createLink("\u2022 Secured [AccessEvaluator]", AccessEvaluatorView.class));
    topBar.add(createLink("\u2022 Secured [Reusable Rule]", ReusableRuleView.class));
    topBar.add(createLink("\u2022 Secured [Runtime Rule]", DynamicRegistrationView.class));
    topBar.add(createLink("\u2022 Unsecured [Security Context During Background Access]",
        BackgroundAccessView.class));
    /* The runtime access rule is only set here for demonstration purposes. */
    RouteAccessConfiguration.forApplicationScope().setAccessRule(DynamicRegistrationView.class,
        AccessRule.of("hasRole('ADMIN')"));
    topBar.add(new Span("|"));


    // https://github.com/vaadin/flow/issues/8720
    topBar.add(new Html("<b>Lit views</b>"));
    topBar.add(createLink("\u2022 Endpoint Access Control", LitView.class));

    topBar.addAndExpand(new Span());

    userLabel = new Label();
    topBar.add(userLabel);
    topBar.add(new Span("|"));

    RouterLink loginLink = new RouterLink("Login", LoginView.class);
    loginLink.setHighlightCondition((t, event) ->
    {
      return VaadinSecurity.check().isAuthenticated();
    });
    loginLink.setHighlightAction((t, highlight) ->
    {
      if (highlight)
        t.getStyle().set("color", "var(--lumo-disabled-text-color)");
      else
        t.getStyle().remove("color");
    });
    topBar.add(loginLink);

    topBar.add(new Span("|"));

    ClickAnchor clickAnchor = new ClickAnchor("Logout");
    clickAnchor.addClickListener(event ->
    {
      VaadinAuthenticationService.get(event.getSource()).logout();
    });
    topBar.add(clickAnchor);
    layout.add(topBar);

    layout.add(new Hr());

    updateLoggedInUser();

    return layout;
  }

  @Override
  public void showRouterLayoutContent(HasElement content)
  {
    getContent().add((Component) content);
  }

  @Override
  public void removeRouterLayoutContent(HasElement oldContent)
  {
    getContent().remove((Component) oldContent);
  }


  @Override
  public void authenticationChange(AuthenticationChangeEvent event)
  {
    updateLoggedInUser();
  }

  private void updateLoggedInUser()
  {
    if (VaadinSecurity.check().isAuthenticated())
    {
      userLabel.setText(
          "logged in as " + SecurityContextHolder.getContext().getAuthentication().getName());
    }
    else
    {
      userLabel.setText("not logged in");
    }
  }

  private RouterLink createLink(String text, Class<? extends Component> navigationTarget)
  {
    RouterLink link = new RouterLink(text, navigationTarget);
    link.setHighlightCondition(HighlightConditions.sameLocation());
    link.setHighlightAction((t, highlight) ->
    {
      if (highlight)
      {
        t.getStyle().set("color", "var(--lumo-success-text-color)");
        t.getStyle().set("text-decoration", "underline");
      }
      else
      {
        t.getStyle().remove("color");
        t.getStyle().remove("text-decoration");
      }
    });
    return link;
  }


  private static class ClickAnchor
    extends
      Anchor
    implements
      ClickNotifier<ClickAnchor>
  {

    public ClickAnchor(String text)
    {
      setText(text);
      getStyle().set("cursor", "pointer");
    }

  }

}
