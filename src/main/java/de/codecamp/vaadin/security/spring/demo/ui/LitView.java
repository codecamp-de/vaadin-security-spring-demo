package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.router.Route;


@Route(value = "lit", layout = AppFrame.class)
@Tag("lit-view")
@JsModule("./views/lit-view.ts")
public class LitView
  extends
    LitTemplate
{
}
