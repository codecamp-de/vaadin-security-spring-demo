package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import java.time.LocalDateTime;


@Route(value = "background", layout = AppFrame.class)
public class BackgroundAccessView
  extends
    Composite<VerticalLayout>
{

  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();

    TextArea resultField = new TextArea();
    layout.setAlignSelf(Alignment.STRETCH, resultField);
    resultField.setReadOnly(true);
    resultField.setLabel("Result");

    layout.add(new Button("Check authentication from background in 5s", e ->
    {
      new Thread(() ->
      {
        try
        {
          Thread.sleep(5000);
        }
        catch (InterruptedException ex)
        {
          ex.printStackTrace();
        }
        e.getSource().getUI().ifPresent(ui -> ui.access(() ->
        {
          if (VaadinSecurity.check().isAuthenticated())
            resultField.setValue(LocalDateTime.now().toString() + ": You are authenticated.");
          else
            resultField.setValue(LocalDateTime.now().toString() + ": You are NOT authenticated.");
        }));
      }).start();
    }));

    layout.add(resultField);

    return layout;
  }

}
