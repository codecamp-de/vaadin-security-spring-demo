package de.codecamp.vaadin.security.spring.demo.config;


import de.codecamp.vaadin.security.spring.config.VaadinSecurityConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
public class WebSecurityConfig
{

  // https://spring.io/blog/2022/02/21/spring-security-without-the-websecurityconfigureradapter

  @Bean
  SecurityFilterChain vaadinSecurityFilterChain(HttpSecurity http)
    throws Exception
  {
    http.apply(new VaadinSecurityConfigurer());
    http.apply(new ApplicationSecurityConfigurer());

    AuthenticationManagerBuilder authMan = http.getSharedObject(AuthenticationManagerBuilder.class);
    authMan.inMemoryAuthentication() //
        .withUser("user").password("{noop}password").roles("USER").and() //
        .withUser("admin").password("{noop}password").roles("ADMIN");

    return http.build();
  }


  static class ApplicationSecurityConfigurer
    extends
      AbstractHttpConfigurer<ApplicationSecurityConfigurer, HttpSecurity>
  {

    @Override
    public void init(HttpSecurity builder)
      throws Exception
    {
      builder.rememberMe();
    }

  }

}
