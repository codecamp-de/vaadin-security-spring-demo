package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.codecamp.vaadin.security.spring.authentication.VaadinAuthenticationService;


@Route(value = "login", layout = AppFrame.class)
public class LoginView
  extends
    Composite<VerticalLayout>
{

  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();

    layout.setSizeFull();
    layout.setAlignItems(Alignment.CENTER);
    layout.setJustifyContentMode(JustifyContentMode.CENTER);


    LoginForm loginForm = new LoginForm();
    layout.add(loginForm);
    loginForm.addLoginListener(event ->
    {
      VaadinAuthenticationService.get().login(this, event.getUsername(), event.getPassword(), false,
          result ->
          {
            loginForm.setEnabled(true);
            loginForm.setError(result.isFailure());
            return false;
          });
    });

    layout.add(new Span("user / password"));
    layout.add(new Span("admin / password"));

    return layout;
  }

}
