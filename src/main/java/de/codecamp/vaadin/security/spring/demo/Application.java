package de.codecamp.vaadin.security.spring.demo;


import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.theme.Theme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Push
@Theme(value = "application")
public class Application
  implements
    AppShellConfigurator
{

  public static void main(String[] args)
  {
    SpringApplication.run(Application.class, args);
  }

}
