package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import de.codecamp.vaadin.security.spring.demo.security.RequiresAdminRole;


@Route(value = "expression-reusable", layout = AppFrame.class)
@RequiresAdminRole
public class ReusableRuleView
  extends
    Composite<VerticalLayout>
{

  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();

    layout.add(new Span("Only admins can see this."));

    return layout;
  }

}
