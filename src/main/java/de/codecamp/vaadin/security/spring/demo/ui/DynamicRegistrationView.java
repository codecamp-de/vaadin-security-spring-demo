package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;


@Route(value = "dynamic", layout = AppFrame.class)
public class DynamicRegistrationView
  extends
    Composite<VerticalLayout>
{

  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();

    layout
        .add(new Span("Only admins can see this. The access rule is set dynamically at runtime."));

    return layout;
  }

}
