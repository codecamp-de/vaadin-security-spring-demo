package de.codecamp.vaadin.security.spring.demo.security;


import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@SecuredAccess("hasRole('ADMIN')")
public @interface RequiresAdminRole
{

}
