package de.codecamp.vaadin.security.spring.demo.endpoints;


import de.codecamp.vaadin.security.spring.access.rules.PermitAll;
import de.codecamp.vaadin.security.spring.access.rules.RequiresAuthentication;
import de.codecamp.vaadin.security.spring.demo.security.RequiresAdminRole;
import dev.hilla.Endpoint;


@Endpoint
@RequiresAuthentication
public class DemoEndpoint
{

  @PermitAll // Spring Security's definition of permitAll: allow annoymous
  public String methodOne(String name)
  {
    return "Hello " + name + "!";
  }

  // rule inherited from class
  public String methodTwo(String name)
  {
    return "Greetings, " + name + "!";
  }

  @RequiresAdminRole // a custom annotation
  public String methodThree(String name)
  {
    return "Good bye, " + name + "!";
  }

}
