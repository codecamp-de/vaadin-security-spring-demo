package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;


@Route(value = "", layout = AppFrame.class)
public class MainView
  extends
    Composite<VerticalLayout>
{

  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();

    layout.add(new Span("Everyone can see this."));

    if (VaadinSecurity.check().isAnonymous())
    {
      layout.add(new Span("Every anonymous/unauthenticated user can see this."));
    }

    if (VaadinSecurity.check().isAuthenticated())
    {
      layout.add(new Span("Every authenticated user can see this."));
    }
    else
    {
      layout.add(new Span("You're not authenticated."));
    }

    if (VaadinSecurity.check().hasRole("ADMIN"))
    {
      layout.add(new Span("Only admins can see this."));
    }
    else
    {
      layout.add(new Span("You're not an admin."));
    }

    if (VaadinSecurity.hasAccessTo(ExpressionView.class))
    {
      layout.add(new RouterLink("Admin View [Expression] (link only visible as admin)",
          ExpressionView.class));
    }

    return layout;
  }

}
