package de.codecamp.vaadin.security.spring.demo.ui;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import de.codecamp.vaadin.security.spring.access.SecuredAccess;


@Route(value = "parameterized", layout = AppFrame.class)
@SecuredAccess("hasRole('ADMIN')")
public class ParameterizedRouteView
  extends
    Composite<VerticalLayout>
  implements
    HasUrlParameter<String>
{

  @Override
  protected VerticalLayout initContent()
  {
    VerticalLayout layout = super.initContent();

    layout.add(new Span("Only admins can see this."));

    return layout;
  }

  @Override
  public void setParameter(BeforeEvent event, String parameter)
  {
    getContent().removeAll();
    getContent().add(new Span(parameter));
  }

}
